using UnityEngine;

public static class Constant
{
    public const string k_playerTag = "Player";

    public const string k_layerDefaut = "Default";
    public const string k_layerNavMeshable = "NavMeshable";

    public const string k_dissolveAmount = "_DissolveAmount";

    public const string k_animatorRun = "Run";
    public const string k_animatorAtk = "Atk";

    public const string k_paramMain = "main";
    public const string k_paramMusic = "music";
    public const string k_paramSfx = "sfx";

    public const string k_playerName = "PlayerName";
    public const string k_playerID = "PlayerID";
    public const string k_lastScore = "LastScore";

    public const string k_menuScene = "Scenes/0_LaSceneDuMenu";
    public const string k_mainScene = "Scenes/1_LaSceneDuJeu";
    public const string k_leaderboardScene = "Scenes/2_LeaderboardScene";

    public const string k_archibald = "Archibald";

    public const float k_waitToDieWhenLit = 0.5f;
}