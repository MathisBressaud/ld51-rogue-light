using UnityEngine;
using TMPro;

public class DisplayLeaderboardEntry : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_rank;
    [SerializeField] private GameObject m_loading;
    [SerializeField] private GameObject m_loadingHiden;
    [Space(10)]
    [SerializeField] private TextMeshProUGUI m_name;
    [SerializeField] private TextMeshProUGUI m_score;

    public void UpdateEntry(int position, string playerName, int score, Color displayColor)
    {
        m_rank.text = position.ToString();
        m_loading.SetActive(false);
        m_loadingHiden.SetActive(true);

        m_rank.color = displayColor;
        m_name.text = playerName;
        m_name.color = displayColor;
        m_score.text = score.ToString();
        m_score.color = displayColor;
    }
}
