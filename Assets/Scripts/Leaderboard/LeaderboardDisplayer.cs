using TMPro;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using LootLocker.Requests;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

public class LeaderboardDisplayer : MonoBehaviour
{
    public const int LeaderboardID = 7510;

    [SerializeField] private bool m_isMenu;

    private bool m_initialized;

    [TabGroup("Leaderboard"), SerializeField] private TMP_InputField m_playerName;

    [TabGroup("Leaderboard"), SerializeField] private ScrollbarManager m_scrollbar;

    [TabGroup("Leaderboard"), SerializeField] private GameObject m_submitZone;

    [TabGroup("Leaderboard"), SerializeField] private GameObject m_buttonMenu;

    [TabGroup("Leaderboard"), SerializeField] private GameObject m_leoText;

    [TabGroup("Leaderboard"), SerializeField] private Image m_blackScreen;

    [PropertySpace]

    [TabGroup("Leaderboard"), SerializeField] private DisplayLeaderboardEntry[] m_displayLeaderboardEntries;

    [TabGroup("DisplayParameters"), SerializeField] private Color m_baseColor;
    [TabGroup("DisplayParameters"), SerializeField] private Color m_playerColor;

    //********  Functions
    void Start()
    {
        m_blackScreen.DOFade(0f, 1f);
        StartCoroutine(LoginRoutine());
    }

    private void OnEnable()
    {
        if (m_initialized)
        {
            UpdateLeaderboard();

            if (PlayerPrefs.GetInt(Constant.k_lastScore, 0) > 0)
                m_submitZone.SetActive(true);
        }
        else
        {
            StartCoroutine(Delay());
        }

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(2f);

            if (m_initialized)
            {
                UpdateLeaderboard();
            }
        }
    }

    public void SubmitScore()
    {
        if (!string.IsNullOrEmpty(m_playerName.text))
        {
            StartCoroutine(SubmitScoreRoutine(PlayerPrefs.GetInt(Constant.k_lastScore, 0)));
            m_submitZone.SetActive(false);
            m_buttonMenu.SetActive(true);

            if (m_playerName.text.Contains(Constant.k_archibald))
            {
                m_leoText.SetActive(true);
            }
        }
    }

    public void UpdateLeaderboard()
    {
        StartCoroutine(FetchLeaderboardRoutine());
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(Constant.k_menuScene);

        //StartCoroutine(LoadAsync());

        //IEnumerator LoadAsync()
        //{
        //    var operation = SceneManager.LoadSceneAsync(Constant.k_menuScene);

        //    operation.allowSceneActivation = false;

        //    m_blackScreen.DOFade(1f, 1f);

        //    yield return new WaitForSeconds(1f);

        //    yield return new WaitUntil(() => operation.progress >= 0.8f);

        //    operation.allowSceneActivation = true;
        //}
    }
  
    //********  Leaderboard Functions
    IEnumerator LoginRoutine()
    {
        bool done = false;
        LootLockerSDKManager.StartGuestSession((response) =>
        {
            if (response.success)
            {
                Debug.Log("Guest Session created");
                PlayerPrefs.SetString(Constant.k_playerID, response.player_id.ToString());
                m_initialized = true;
                done = true;
            }
            else
            {
                Debug.Log("Guest Session failed to create");
                done = true;
            }
        });
        yield return new WaitUntil(() => done == true);

        if (m_initialized)
            UpdateLeaderboard();
    }

    IEnumerator SubmitScoreRoutine(int scoreToAdd)
    {
        bool done = false;

        PlayerPrefs.SetString(Constant.k_playerName, m_playerName.text);

        LootLockerSDKManager.SubmitScore(PlayerPrefs.GetString(Constant.k_playerID), scoreToAdd, LeaderboardID, (response) =>
        {
            if (response.success)
            {
                LootLockerSDKManager.SetPlayerName(m_playerName.text, (responseName) => { });
                Debug.Log(scoreToAdd);
                done = true;
            }
            else
            {
                Debug.Log("Failed to submit score");
                done = true;
            }
        });

        yield return new WaitUntil(() => done == true);
        yield return new WaitForSeconds(1f);
        UpdateLeaderboard();
    }
    IEnumerator FetchLeaderboardRoutine()
    {
        string lastName = PlayerPrefs.GetString(Constant.k_playerName, string.Empty);
        m_playerName.text = lastName;

        bool done = false;
        string playerName = string.Empty;
        LootLockerSDKManager.GetPlayerName((response) =>
        {
            if (response.success)
                playerName = response.name;
        });

        LootLockerSDKManager.GetScoreList(LeaderboardID, m_displayLeaderboardEntries.Length, 0, (response) =>
        {
            if (response.success)
            {
                LootLockerLeaderboardMember[] members = response.items;
                for (int i = 0; i < members.Length; i++)
                {
                    m_displayLeaderboardEntries[i].UpdateEntry(i + 1, members[i].player.name, members[i].score, (members[i].player.name == playerName? m_playerColor : m_baseColor));

                    if (members[i].player.name == playerName)
                    {
                        m_scrollbar.scrollbar.value = ((float)Mathf.Max(0, i - 1) / (float)m_displayLeaderboardEntries.Length);
                        m_scrollbar.OnValueChange();
                    }
                }
            }
        });

        yield return null;
    }
}