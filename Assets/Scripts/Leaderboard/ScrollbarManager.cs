using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollbarManager : MonoBehaviour
{
    public Scrollbar scrollbar;
    public RectTransform scrollPanel;
    Vector2 panelPos;


    public float maxHeigh;
    public float minHeigh;


    public void OnValueChange()
    {
        panelPos.y = maxHeigh * scrollbar.value;

        scrollPanel.anchoredPosition = panelPos;
    }
}
