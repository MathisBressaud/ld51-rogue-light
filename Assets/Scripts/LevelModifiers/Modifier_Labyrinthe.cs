using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Modifier", menuName = "Upgrade/Modifier level/labyrinthe")]
public class Modifier_Labyrinthe : LvlModifier
{
    public bool isV2;
    public override void ApplyLevelModifier(GameObject labyrinthe, GameObject labyrinthe2)
    {
        labyrinthe.SetActive(true);
        labyrinthe2.SetActive(isV2);
    }
}
