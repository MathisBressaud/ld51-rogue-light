using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Modifier", menuName = "Upgrade/Modifier level/Lazer")]
public class Modifier_Lazer : LvlModifier
{
    public bool isInfinite;
    public override void ApplyLevelModifier(Lazer myLazer)
    {
        myLazer.gameObject.SetActive(true);
        myLazer.SetLazer(isInfinite);
    }
}
