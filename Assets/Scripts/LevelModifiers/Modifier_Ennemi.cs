using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Modifier", menuName = "Upgrade/Modifier level/ennemies")]
public class Modifier_Ennemi : LvlModifier
{
    public float bonus_Range;
    [Space(10)]
    public float bonus_StartTime;
    public int bonus_Projectile;

    public override void ApplyLevelModifier(Enemy mob)
    {
        mob.m_attackRange_bonus += bonus_Range;
        mob.m_attackStartWaitTime_bonus += bonus_StartTime;
        mob.m_projectileNumber_bonus += bonus_Projectile;
    }
}
