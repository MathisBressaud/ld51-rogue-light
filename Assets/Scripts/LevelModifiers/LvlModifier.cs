using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//[CreateAssetMenu(fileName = "Modifier", menuName = "Upgrade/Modifier level")]
public class LvlModifier : ScriptableObject
{
    public virtual void ApplyLevelModifier()
    {

    }

    public virtual void ApplyLevelModifier(Lazer myLazer)
    {

    }

    public virtual void ApplyLevelModifier(GameObject labyrinthe, GameObject labyrinthe2)
    {

    }

    public virtual void ApplyLevelModifier(Enemy mob)
    {

    }
}
