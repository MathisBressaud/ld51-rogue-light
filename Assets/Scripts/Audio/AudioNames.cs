public static class AudioNames
{
    public const string k_footstep = "SFX_Footstep";
    public const string k_dash = "Dash";

    public const string k_fireStart = "Fire_Start";
    public const string k_matchStart = "MecheFireStart";
    public const string k_matchConstant = "MecheConstant";

    public const string k_bdfStart = "TirBDF";
    public const string k_bdfConstant = "BDFConstant";
    public const string k_lanceFlammeConstant = "LanceFlammeConstant";

    public const string k_laser = "WaterLaserSound";

    public const string k_shootEnemy = "ShootEnemy";
    public const string k_hitWater = "HitProjectileWater";

    public const string k_plouf = "Plouf";

    public const string k_tension = "TensionFX";
}
