using System.Collections;
using UnityEngine;
using DG.Tweening;

public class GooglyFall : MonoBehaviour
{
    [SerializeField] private float m_duration = 3f;

    [SerializeField] private float m_shakeStrength = .2f;

    [SerializeField] private float m_fadeOutDuration = .5f;

    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(Delay());

        transform.eulerAngles = new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
        transform.localScale = Vector3.one;

        transform.DOShakePosition(0.1f, m_shakeStrength).Play();

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(m_duration);

            transform.DOScale(Vector3.zero, m_fadeOutDuration).Play().OnComplete(() => { gameObject.SetActive(false); });
        }
    }
}
