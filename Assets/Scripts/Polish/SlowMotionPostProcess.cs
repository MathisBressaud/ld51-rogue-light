using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using DG.Tweening;
using Sirenix.OdinInspector;

public class SlowMotionPostProcess : MonoBehaviour
{
    [TabGroup("Settings"),SerializeField] float slowMotionTimeIn = 0.25f;
    [TabGroup("Settings"), SerializeField] float slowMotionTimeOut = 0.25f;

    [Title("References")]
    [SerializeField] Volume slowMotionVolume;

    Tween m_slowMoTween;

    [Button("START SLOW MOTION VOLUME")]
    public void StartSlowMotionVolume()
    {
        m_slowMoTween?.Kill();
        m_slowMoTween = DOTween.To(() => slowMotionVolume.weight, x => slowMotionVolume.weight = x, 1, slowMotionTimeIn);
    }

    [Button("STOP SLOW MOTION VOLUME")]
    public void StopSlowMotionVolume()
    {
        m_slowMoTween?.Kill();
        m_slowMoTween = DOTween.To(() => slowMotionVolume.weight, x => slowMotionVolume.weight = x, 0, slowMotionTimeOut);
    }
}
