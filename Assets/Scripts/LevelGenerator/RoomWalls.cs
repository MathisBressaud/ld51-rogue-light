using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomWalls : MonoBehaviour
{
    public GameObject[] BottomWalls;
    public GameObject[] TopWalls;
    public Transform[] CorridorTransforms;

    public GameObject Corridor;

    public void ResetRoomWalls()
    {
        for (int i = 0; i < BottomWalls.Length; i++)
        {
            BottomWalls[i].SetActive(true);
        }

        for (int i = 0; i < TopWalls.Length; i++)
        {
            TopWalls[i].SetActive(true);
        }
    }
}
