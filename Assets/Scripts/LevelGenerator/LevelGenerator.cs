using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class LevelGenerator : MonoBehaviour
{
    [Title("SETTINGS")]
    [SerializeField] Pool m_roomWalls;
    [SerializeField] Pool m_roomBonus;
    [SerializeField] List<Pool> m_roomsList = new List<Pool>();

    [Space(10)]
    [TabGroup("LEVEL GENERATOR"), SerializeField] float roomGenerationDistance = 40f;
    [TabGroup("LEVEL GENERATOR"), SerializeField] int roomDeleteOffset = 3;
    [TabGroup("LEVEL GENERATOR"), SerializeField] int roomBonusSpawnRate = 4;

    [Title("Debug")]
    [SerializeField] List<RoomWalls> m_roomWallsList = new List<RoomWalls>();


    Vector3 m_currentRoomPos;
    GameObject m_newRoom;

    private void Start()
    {
        m_currentRoomPos = m_roomWallsList[m_roomWallsList.Count - 1].transform.parent.position;
    }

    [Button("CREATE NEW ROOM")]
    public void CreateNewRoom()
    {
        //UPGRADE => ADAPTER DIRECTION A LA PORTE DE LA DERNIERE SALLE MEME SUR LES COTES + TWEAK VALEUR SI SALLE PAS CARR�E
        m_currentRoomPos = m_roomWallsList[m_roomWallsList.Count - 1].transform.parent.position + (Vector3.forward * roomGenerationDistance);

        //----------------- WALLS ----------------------------------------------------------------------------------------------------------

        GameObject newWalls = m_roomWalls.Instantiate(m_currentRoomPos, Quaternion.identity); //POOL WALLS
        RoomWalls roomWalls = newWalls.GetComponent<RoomWalls>();

        //Remove le mur du bas correspondant au mur du haut de l'ancienne salle
        //UPGRADE => CHECK SI LE MUR ETAIT A DROITE / BAS / GAUCHE 
        if (m_roomWallsList.Count >= 0)
        {
            for (int i = 0; i < m_roomWallsList[m_roomWallsList.Count - 1].TopWalls.Length; i++)
            {
                if (!m_roomWallsList[m_roomWallsList.Count - 1].TopWalls[i].gameObject.activeInHierarchy)
                {
                    roomWalls.BottomWalls[i].gameObject.SetActive(false);
                }
            }
        }

        m_roomWallsList.Add(roomWalls);

        //Remove random mur du haut de la nouvelle salle
        //UPGRADE => PRENDRE UN MUR RANDOM ENTRE GAUCHE / HAUT / DROITE
        int randomDeleteWallIndex = Random.Range(0, roomWalls.TopWalls.Length);
        roomWalls.TopWalls[randomDeleteWallIndex].gameObject.SetActive(false);

        //Placement du Couloir
        roomWalls.Corridor.transform.position = roomWalls.CorridorTransforms[randomDeleteWallIndex].transform.position;
        roomWalls.Corridor.transform.forward = roomWalls.CorridorTransforms[randomDeleteWallIndex].transform.forward;


        //----------------- ROOM ----------------------------------------------------------------------------------------------------------

        //Debug.Log("spawn rate modulo = " + m_roomWallsList.Count % roomBonusSpawnRate);

        //int randomRoomIndex = Random.Range(0, m_roomsList.Count);
        //GameObject newRoom; = m_roomsList[randomRoomIndex].Instantiate(m_currentRoomPos, Quaternion.identity); //POOL ROOM

        if(m_roomWallsList.Count % roomBonusSpawnRate == 0) //Spawn Room Bonus selon le spawnRate
        {
            m_newRoom = m_roomBonus.Instantiate(m_currentRoomPos, Quaternion.identity); //POOL ROOM
        }
        else
        {
            int randomRoomIndex = Random.Range(0, m_roomsList.Count);
            m_newRoom = m_roomsList[randomRoomIndex].Instantiate(m_currentRoomPos, Quaternion.identity); //POOL ROOM
        }

        //newWalls.transform.parent = newRoom.transform;
        newWalls.transform.parent = m_newRoom.transform;

        //Delete des salles trop loin
        if (m_roomWallsList.Count >= roomDeleteOffset
        && m_roomWallsList[m_roomWallsList.Count - roomDeleteOffset].gameObject.activeInHierarchy)
            DeleteRoom(m_roomWallsList.Count - roomDeleteOffset);
    }

    void DeleteRoom(int index)
    {
        m_roomWallsList[index].ResetRoomWalls();
        m_roomWallsList[index].transform.parent.gameObject.SetActive(false);
    }

}
