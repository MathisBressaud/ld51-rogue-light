﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Collections;

public class AudioSettings : MonoBehaviour
{
    [SerializeField] AudioMixer m_mixer;

    [SerializeField] Slider mainVolumeSlider;
    [SerializeField] Slider musicVolumeSlider;
    [SerializeField] Slider sfxVolumeSlider;

    bool isProcessing;

    private void OnEnable()
    {
        StartCoroutine(SetSliders());
    }

    public void SetVolume()
    {
        if (isProcessing)
        {
            m_mixer.SetFloat(Constant.k_paramMain, Mathf.Log10(mainVolumeSlider.value) * 20);
            m_mixer.SetFloat(Constant.k_paramMusic, Mathf.Log10(musicVolumeSlider.value) * 20);
            m_mixer.SetFloat(Constant.k_paramSfx, Mathf.Log10(sfxVolumeSlider.value) * 20);
        }
    }

    IEnumerator SetSliders ()
    {
        //C'est un peu dégeu mais sinon ça bug
        isProcessing = false;

        mainVolumeSlider.value = GetMixerLevel(Constant.k_paramMain);

        yield return new WaitForEndOfFrame();

        musicVolumeSlider.value = GetMixerLevel(Constant.k_paramMusic);

        yield return new WaitForEndOfFrame();

        sfxVolumeSlider.value = GetMixerLevel(Constant.k_paramSfx);

        isProcessing = true;
    }

    float GetMixerLevel(string floatName)
    {
        float value = 0;
        bool result = m_mixer.GetFloat(floatName, out value);

        if (result)
        {
            value = Mathf.Exp(value);
            return value;
        }
        else
        {
            return 0f;
        }
    }
}
