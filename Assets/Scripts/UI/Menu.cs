using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Menu : MonoBehaviour
{
    public Image m_blackScreen;

    private void Start()
    {
        //m_blackScreen.DOFade(0f, 1f);
    }

    public void Play()
    {
        StartCoroutine(LoadAsync());

        IEnumerator LoadAsync()
        {
            //var operation = SceneManager.LoadSceneAsync(Constant.k_mainScene);

            //operation.allowSceneActivation = false;

            m_blackScreen.DOFade(1f, 1f);

            yield return new WaitForSeconds(1f);

            SceneManager.LoadScene(Constant.k_mainScene);

            //yield return new WaitUntil(() => operation.progress >= 0.9f);

            //operation.allowSceneActivation = true;
        }
    }

    public void Quit()
    {
        Application.Quit();
    }
}
