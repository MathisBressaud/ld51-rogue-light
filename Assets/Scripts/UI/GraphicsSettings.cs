﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

public class GraphicsSettings : MonoBehaviour
{
    //Quality
    [SerializeField] TMP_Dropdown quality_Dropdown;

    TMP_Dropdown.OptionData m_NewData, m_NewData2;

    List<TMP_Dropdown.OptionData> m_Messages = new List<TMP_Dropdown.OptionData>();

    //ScreenSize
    [SerializeField] TMP_Dropdown resolution_Dropdown;

    List<TMP_Dropdown.OptionData> m_Resolutions = new List<TMP_Dropdown.OptionData>();

    Resolution[] resolutions;

    [SerializeField] Toggle fullscreen_toggle;

    [SerializeField] Toggle VSync_toggle;


    private void Start()
    {
        SetDropdownMenu();
        SetResolutionMenu();
        SetFullScreenToggle();
        SetVSyncToggle();
    }

    void SetDropdownMenu()
    {
        int selection = QualitySettings.GetQualityLevel();
        string[] names = QualitySettings.names;
        for (int i = 0; i < names.Length; i++)
        {
            m_NewData = new TMP_Dropdown.OptionData();
            m_NewData.text = names[i];
            m_Messages.Add(m_NewData);
        }
        quality_Dropdown.ClearOptions();
        quality_Dropdown.AddOptions(m_Messages);
        quality_Dropdown.value = selection;
    }

    public void ApplyQuality()
    {
        QualitySettings.SetQualityLevel(quality_Dropdown.value, true);
        SetVSyncToggle();
    }

    void SetResolutionMenu()
    {
        int selection = 0;
        resolutions = Screen.resolutions;
        for (int i = 0; i < resolutions.Length; i++)
        {
            m_NewData2 = new TMP_Dropdown.OptionData();
            m_NewData2.text = resolutions[i].ToString();
            m_Resolutions.Add(m_NewData2);
            if (resolutions[i].ToString() == Screen.currentResolution.ToString())
                selection = i;
        }
        resolution_Dropdown.ClearOptions();
        resolution_Dropdown.AddOptions(m_Resolutions);
    }

    void SetFullScreenToggle()
    {
        fullscreen_toggle.isOn = Screen.fullScreen;
    }

    public void ApplyResolution()
    {
        Resolution currentResolution = resolutions[resolution_Dropdown.value];
        Screen.SetResolution(currentResolution.width, currentResolution.height, fullscreen_toggle.isOn, currentResolution.refreshRate);
        SetVSyncToggle();
    }

    void SetVSyncToggle()
    {
        VSync_toggle.isOn = VSyncActiveState(QualitySettings.vSyncCount);
    }

    public void ApplyVSync()
    {
        if (VSync_toggle.isOn)
            QualitySettings.vSyncCount = 1;
        else
            QualitySettings.vSyncCount = 0;
    }

    bool VSyncActiveState(int value)
    {
        if (value <= 0)
            return false;
        else
            return true;
    }

}
