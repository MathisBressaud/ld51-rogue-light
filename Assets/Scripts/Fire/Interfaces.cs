public interface IFlammable
{
    bool IsFlamed { get; set; }
    void Reset();
    void Lit();
}