using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireAura : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        IFlammable target = other.GetComponent<IFlammable>();

        if (target != null)
            target.Lit();
    }
}
