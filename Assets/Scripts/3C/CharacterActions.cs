using UnityEngine;
using InControl;

public class CharacterActions : PlayerActionSet
{
    public PlayerAction Left;
    public PlayerAction Right;
    public PlayerAction Down;
    public PlayerAction Up;
    public PlayerTwoAxisAction Direction;

    public PlayerAction Dash;
    public PlayerAction Attack;
    public PlayerAction Pause;

    public PlayerAction Aim_Left;
    public PlayerAction Aim_Right;
    public PlayerAction Aim_Down;
    public PlayerAction Aim_Up;
    public PlayerTwoAxisAction Aim;

    public CharacterActions()
    {
        Left = CreatePlayerAction("Left");
        Right = CreatePlayerAction("Right");
        Down = CreatePlayerAction("Down");
        Up = CreatePlayerAction("Up");
        Direction = CreateTwoAxisPlayerAction(Left, Right, Down, Up);

        Dash = CreatePlayerAction("Dash");
        Attack = CreatePlayerAction("Attack");
        Pause = CreatePlayerAction("Pause");

        Left.AddDefaultBinding(Key.A);
        Left.AddDefaultBinding(Key.Q);
        Left.AddDefaultBinding(InputControlType.LeftStickLeft);

        Right.AddDefaultBinding(Key.D);
        Right.AddDefaultBinding(InputControlType.LeftStickRight);

        Down.AddDefaultBinding(Key.S);
        Down.AddDefaultBinding(InputControlType.LeftStickDown);

        Up.AddDefaultBinding(Key.W);
        Up.AddDefaultBinding(Key.Z);
        Up.AddDefaultBinding(InputControlType.LeftStickUp);

        Dash.AddDefaultBinding(Key.Space);
        Dash.AddDefaultBinding(InputControlType.Action1);
        Dash.AddDefaultBinding(InputControlType.LeftTrigger);

        Attack.AddDefaultBinding(Mouse.LeftButton);
        Attack.AddDefaultBinding(InputControlType.Action3);
        Attack.AddDefaultBinding(InputControlType.RightTrigger);

        Pause.AddDefaultBinding(Key.Escape);
        Pause.AddDefaultBinding(InputControlType.Command);

        Aim_Left = CreatePlayerAction("Aim_Left");
        Aim_Right = CreatePlayerAction("Aim_Right");
        Aim_Down = CreatePlayerAction("Aim_Down");
        Aim_Up = CreatePlayerAction("Aim_Up");
        Aim = CreateTwoAxisPlayerAction(Aim_Left, Aim_Right, Aim_Down, Aim_Up);

        Aim_Left.AddDefaultBinding(InputControlType.RightStickLeft);
        Aim_Right.AddDefaultBinding(InputControlType.RightStickRight);
        Aim_Down.AddDefaultBinding(InputControlType.RightStickDown);
        Aim_Up.AddDefaultBinding(InputControlType.RightStickUp);
    }
}
