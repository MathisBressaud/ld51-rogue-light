using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.Windows;

public class BouleDeFeu : MonoBehaviour
{
    Rigidbody rb;
    Vector3 startPoint;




    [TabGroup("BDF params")] public float bdf_PostHitLifeTime;
    [Space(10)]
    [TabGroup("BDF params")] public float bdf_MoveSpeed;
    [TabGroup("BDF params")] public float bdf_Range;
    [Space(10)]
    [TabGroup("BDF params")] public float bdf_BaseScale;
    [Space(10)]
    [TabGroup("BDF params")] public float bdf_bonus_MoveSpeed;
    [TabGroup("BDF params")] public float bdf_bonus_Scale;



    [TabGroup("BDF VFX")] public GameObject[] bdf_Renders;
    [TabGroup("BDF VFX")] public SphereCollider bdf_Col;
    [Space(10)]
    [TabGroup("BDF VFX")] public ParticleSystem bdf_sparks;
    [TabGroup("BDF VFX")] public TrailRenderer bdf_trail;
    [Space(10)]
    [TabGroup("BDF VFX")] public float bdf_VFX_lifeTime;

    float defaultTrailWidth;

    public UnityEvent impact;

    private AudioSource m_source;

    bool pierceBDF;

    // **************************************** Start
    private void Start()
    {
        Init();
        SetBDF();
    }

    void Init()
    {
        rb = GetComponent<Rigidbody>();
        defaultTrailWidth = bdf_trail.startWidth;
    }

    public void SetBDF()
    {
        reachTravelDist = false;
        rb.isKinematic = false;

        transform.localScale = Vector3.one * bdf_BaseScale;

        bdf_trail.startWidth = defaultTrailWidth * (bdf_BaseScale + bdf_bonus_Scale);
        bdf_trail.emitting = true;

        foreach (GameObject gO in bdf_Renders)
            gO.SetActive(true);
        bdf_Col.enabled = true;
    }

    public void ResetBDF()
    {
        SetBDF();
        m_source.Stop();
        gameObject.SetActive(false);
        impactTriggerOnce = stopMoving = false;
    }

    // **************************************** Update
    private void Update()
    {
        TravelTime();
    }

    bool reachTravelDist;
    void TravelTime()
    {

        rb.velocity = transform.forward * (bdf_MoveSpeed + bdf_bonus_MoveSpeed) * (stopMoving ? 0 : 1);

        if (Vector3.Distance(startPoint, transform.position) >= bdf_Range)
        {
            if(!reachTravelDist)
                BDFimpact();
        }
    }

    bool impactTriggerOnce;
    bool stopMoving;
    private void OnTriggerEnter(Collider other)
    {
        IFlammable target = other.GetComponent<IFlammable>();

        if (target != null && !target.IsFlamed)
        {
            target.Lit();

            if (!pierceBDF && !impactTriggerOnce)
            {
                impactTriggerOnce = true;
                StartCoroutine(DelayLifeTime());
                stopMoving = true;
                StartCoroutine(DelayLifeTime());

                IEnumerator DelayLifeTime()
                {

                    yield return new WaitForSeconds(bdf_PostHitLifeTime);
                    BDFimpact();
                }

            }
        }
    }


    // **************************************** Set BDF
    public void SetupBDF(Vector3 spawnPoint, Vector3 dir, float bonus_Speed, float bonus_Scale, bool pierce)
    {
        transform.position = startPoint = spawnPoint;
        transform.DOLookAt(dir, 0).Play();

        bdf_bonus_MoveSpeed = bonus_Speed;
        transform.localScale = Vector3.one * (bdf_BaseScale + bonus_Scale);

        bdf_bonus_Scale = bonus_Scale;

        pierceBDF = pierce;

        if(m_source)
        {
            m_source.volume = 1;
            m_source.Play();
        }
        else
            m_source = AudioManager.instance.PlaySFXLoop(AudioNames.k_bdfConstant);
    }


    // **************************************** SetupImpact
    public void BDFimpact()
    {
        reachTravelDist = true;

        impact.Invoke();

        m_source.DOFade(0, 0.2f).Play().OnComplete(() =>
        {
            m_source.Stop();
        });

        bdf_sparks.Stop();
        bdf_trail.emitting = false;

        rb.isKinematic = true;
        foreach (GameObject gO in bdf_Renders)
            gO.SetActive(false);
        bdf_Col.enabled = false;

        StartCoroutine(Delay());

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(bdf_VFX_lifeTime);
            ResetBDF();
        }
    }


}
