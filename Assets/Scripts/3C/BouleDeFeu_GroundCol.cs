using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouleDeFeu_GroundCol : MonoBehaviour
{
    public BouleDeFeu bdf;

    private void OnTriggerEnter(Collider other)
    {
        bdf.BDFimpact();
    }
}
