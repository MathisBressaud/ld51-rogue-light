using Cinemachine;
using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TouchExample;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class MatchController : MonoBehaviour
{
    bool isDead;

    public bool useController;

    UImanager uiManager;

    CharacterActions inputs;
    Rigidbody rb;
    Camera cam;
    Animator anim;
    NavMeshAgent agent;

    public Transform meshes;
    public bool isInvulnerable;
    [Space(10)]

    [TabGroup("Chara Movement")] public float movespeed;
    [Space(10)]
    [TabGroup("Chara Movement")] public float bonus_movespeed;
   
    Vector3 dir;
    Vector3 Lastdir;


    [TabGroup("I'm On Fiiiiiiire")] public Transform fireAura;
    [TabGroup("I'm On Fiiiiiiire")] public float baseAuraScale;
    [Space(10)]
    [TabGroup("I'm On Fiiiiiiire")] public float bonus_AuraScale;


    [TabGroup("El Dashos del alumettos")] public LayerMask layerDash;
    [Space(10)]
    [TabGroup("El Dashos del alumettos")] public float dashRange;
    [TabGroup("El Dashos del alumettos")] public float dashDuration;
    [TabGroup("El Dashos del alumettos")] public Ease dashEase;
    [Space(10)]
    [TabGroup("El Dashos del alumettos")] public float dashHighFrames;
    [TabGroup("El Dashos del alumettos")] public float dashCD;
    [Space(10)]
    [TabGroup("El Dashos del alumettos")] public float bonus_dashCD;
    [Space(10)]
    [TabGroup("El Dashos del alumettos")] public Image dashCd;

    bool canDash = true;
    bool isDashing;


    [TabGroup("El Boulass de le FIRE")] public Pool bdfPool;
    [TabGroup("El Boulass de le FIRE")] public LayerMask layer;
    [Space(10)]
    [TabGroup("El Boulass de le FIRE")] public Material arrowCD;
    [Space(10)]
    [TabGroup("El Boulass de le FIRE")] public Transform LauncherOrienter;
    [TabGroup("El Boulass de le FIRE")] public Transform LauncherSpawn;
    [TabGroup("El Boulass de le FIRE")] public Transform LauncherDir;
    [Space(10)]
    [TabGroup("El Boulass de le FIRE")] public float bdfCD;
    [Space(10)]
    [TabGroup("El Boulass de le FIRE")] public float bonus_bdfCD;
    [TabGroup("El Boulass de le FIRE")] public float bonus_bdfSpeed;
    [TabGroup("El Boulass de le FIRE")] public float bonus_bdfScale;


    [TabGroup("Other Upgrades")] public float bonus_bulletTime;
    [Space(10)]
    [TabGroup("Other Upgrades")] public bool bonus_flameThrow;
    [TabGroup("Other Upgrades")] public Transform flameThrow_hitbox;
    [TabGroup("Other Upgrades")] public float baseFlameThrowScale;
    [Space(10)]
    [TabGroup("Other Upgrades")] public bool bonus_fireTrail;
    [TabGroup("Other Upgrades")] public Transform[] bonus_fireAura = new Transform[0];
    [TabGroup("Other Upgrades")] public float fireTrailTimingn;
    int trailIndex;
    [Space(10)]
    [TabGroup("Other Upgrades")] public bool bonus_bdfPierce;
    [Space(10)]
    [TabGroup("Other Upgrades")] public bool bonus_bdfTriple;
    [TabGroup("Other Upgrades")] public Transform[] bonus_LauncherSpawn = new Transform[2];
    [TabGroup("Other Upgrades")] public Transform[] bonus_LauncherDir = new Transform[2];
    [Space(10)]
    [TabGroup("Other Upgrades")] public bool bonus_match;
    [TabGroup("Other Upgrades")] public bool bonus_shield;


    RaycastHit ray;
    RaycastHit rayDash;

    Vector3 LastAimPoint;
    bool canBDF = true;

    [Title("Visuels")]
    public Material fireMaterial;
    public Transform firePoint;
    [Space(10)]
    public GameObject[] onDeathDepop = new GameObject[0];
    [Space(10)]
    public CinemachineVirtualCamera deathVcam;


    private AudioSource m_source;

    [Title("Current Upgrades")]
    public List<CharaUpgrade> currentUpgrades = new List<CharaUpgrade>();


    // **************************************** Start
    void Start()
    {
        Init();
        ResetChara();
    }

    void Init()
    {
        inputs = new CharacterActions();
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
        cam = Camera.main;

        anim = GetComponentInChildren<Animator>();

        uiManager = FindObjectOfType<UImanager>();

        Cursor.lockState = CursorLockMode.Confined;
    }

    public void ResetChara()
    {
        canDash = true;
        agent.enabled = false;

        foreach (Transform t in bonus_fireAura)
            t.parent = null;

        ResetUpgrade();
        StartCoroutine(FireTrailCoroutine());
    }



    // **************************************** Update
    void Update()
    {
        InputCheck();
        MoveAction();

        DashAction();
        AttackAction();

        ApplyVisuel();
    }

    void InputCheck()
    {
        if (isDead)
            return;

        dir.x = inputs.Direction.X;
        dir.z = inputs.Direction.Y;

        anim.SetBool("Run", dir.magnitude > 0);
        Lastdir = dir.magnitude > 0 ? dir : Lastdir;

        meshes.DOLookAt(meshes.position + Lastdir, 0).Play();

        InputSwaper();
        AimPointOrienter();
    }

    void AimPointOrienter()
    {
        if (useController)
        {
            if (inputs.Aim.Value.magnitude > 0.35f)
            {
                LastAimPoint = transform.position;
                LastAimPoint.x += inputs.Aim.X;
                LastAimPoint.z += inputs.Aim.Y;

                LauncherOrienter.DOLookAt(LastAimPoint, 0).Play();
            }
        }
        else
        {
            if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out ray, Mathf.Infinity, layer))
            {
                LastAimPoint = ray.point;
                LastAimPoint.y = LauncherOrienter.position.y;

                LauncherOrienter.DOLookAt(LastAimPoint, 0).Play();
            }
        }
    }

    void InputSwaper()
    {
        if (useController)
        {
            if(Input.GetMouseButton(0))
                useController = false;
        }
        else
        {
            if (inputs.Aim.Value.magnitude != 0)
                useController = true;
        }

        Cursor.visible = !useController;
    }


    // **************************************** Le Move
    void MoveAction()
    {
        if (isDashing || isDead)
            return;

        Vector3 currentVel = dir.normalized * (movespeed + bonus_movespeed);
        currentVel.y = rb.velocity.y;

        rb.velocity = currentVel;
    }

    // **************************************** Le Dash
    void DashAction()
    {
        if (isDead)
            return;

        if (inputs.Dash.WasPressed && canDash)
        {
            StartCoroutine(SetDashHighFrames());
            StartCoroutine(SetDashCD());

            DashSequence();
        }

    }

    void DashSequence()
    {
        anim.SetTrigger("Dash");
        isDashing = true;

        //***************** LA
        Vector3 dest = (Physics.Raycast(transform.position + Vector3.up, Lastdir, out rayDash, dashRange, layerDash)) ? rayDash.point : transform.position + (Lastdir.normalized * dashRange);
        dest.y = transform.position.y;

        agent.enabled = true;
        agent.SetDestination(dest);

        AudioManager.instance.PlaySFXOnce(AudioNames.k_dash, 1);

        transform.DOMove(agent.destination, dashDuration * Time.timeScale).SetEase(dashEase).Play().OnComplete(() =>
        {
            isDashing = false;
        });
        agent.enabled = false;
    }


    IEnumerator SetDashHighFrames()
    {
        isInvulnerable = true;
        yield return new WaitForSecondsRealtime(dashHighFrames); 
        isInvulnerable = false;
    }

    IEnumerator SetDashCD()
    {
        canDash = false;

        dashCd.DOKill();
        dashCd.DOFade(1, 0.1f).SetEase(Ease.InOutQuad).Play();
        dashCd.fillAmount = 0;
        dashCd.DOFillAmount(1, dashCD + bonus_dashCD).SetEase(Ease.InOutQuad).Play();

        yield return new WaitForSecondsRealtime(dashCD + bonus_dashCD);
        dashCd.DOFade(0, 0.25f).SetEase(Ease.InOutQuad).Play();

        canDash = true;
    }


    // **************************************** L'Attack
    void AttackAction()
    {
        if (isDead)
            return;

        if (inputs.Attack.IsPressed && !isDashing && canBDF)
        {
            StartCoroutine(SetBDFCD());

            AimPointOrienter();

            AudioManager.instance.PlaySFXOnce(AudioNames.k_bdfStart, 5);

            BouleDeFeu current = bdfPool.GetAvailableObject().GetComponent<BouleDeFeu>();

            current.gameObject.SetActive(true);
            current.SetupBDF(LauncherSpawn.position, LauncherDir.position, bonus_bdfSpeed, bonus_bdfScale, bonus_bdfPierce);

            if (bonus_bdfTriple)
                for(int i = 0; i < bonus_LauncherSpawn.Length; i++)
                {
                    current = bdfPool.GetAvailableObject().GetComponent<BouleDeFeu>();
                    current.gameObject.SetActive(true);
                    current.SetupBDF(bonus_LauncherSpawn[i].position, bonus_LauncherDir[i].position, bonus_bdfSpeed, bonus_bdfScale, bonus_bdfPierce);
                }
        }
    }

    IEnumerator SetBDFCD()
    {
        canBDF = false;

        float value = 0;
        DOTween.To(() => value, x => value = x, 1, bdfCD + bonus_bdfCD).SetEase(Ease.InQuad).Play().OnUpdate(() =>
        {
            arrowCD.SetFloat("_VALUE", value);
        });

        yield return new WaitForSeconds(bdfCD + bonus_bdfCD);
        canBDF = true;
    }

    // **************************************** L'Attack
    public void SetNewUpgrade(CharaUpgrade newUp)
    {
        currentUpgrades.Add(newUp);

        ResetUpgrade();
        ApplyUpgrades();
    }

    void ResetUpgrade()
    {
        bonus_movespeed = 0;
        bonus_AuraScale = 0;
        bonus_dashCD = 0;

        bonus_bdfCD = 0;
        bonus_bdfSpeed = 0;
        bonus_bdfScale = 0;

        bonus_bulletTime = 0;

        bonus_flameThrow = false;
        fireAura.gameObject.SetActive(true);
        flameThrow_hitbox.gameObject.SetActive(false);

        bonus_fireTrail = false;

        bonus_bdfPierce = false;
        bonus_bdfTriple = false;

        bonus_match = false;
        bonus_shield = false;
    }

    void ApplyUpgrades()
    {
        bool hadMatch = bonus_match;
        bool hadShield = bonus_shield;

        for (int i = 0; i < currentUpgrades.Count; i++)
        {
            bonus_movespeed += currentUpgrades[i].bonus_movespeed;
            bonus_AuraScale += currentUpgrades[i].bonus_AuraScale;
            bonus_dashCD += currentUpgrades[i].bonus_dashCD;

            bonus_bdfCD += currentUpgrades[i].bonus_bdfCD;
            bonus_bdfSpeed += currentUpgrades[i].bonus_bdfSpeed;
            bonus_bdfScale += currentUpgrades[i].bonus_bdfScale;

            bonus_bulletTime += currentUpgrades[i].bonus_sloMo;

            bonus_flameThrow = currentUpgrades[i].bonus_flameThrower ? true : bonus_flameThrow;
            bonus_fireTrail = currentUpgrades[i].bonus_fireTrail ? true : bonus_fireTrail;

            bonus_bdfPierce = currentUpgrades[i].bonus_piercing ? true : bonus_bdfPierce;
            bonus_bdfTriple = currentUpgrades[i].bonus_triple ? true : bonus_bdfTriple;

            bonus_match = currentUpgrades[i].bonus_Match ? true : bonus_match;
            bonus_shield = currentUpgrades[i].bonus_Shield ? true : bonus_shield;
        }

        if (bonus_match && !hadMatch)
            uiManager.ObtainGameplayIcon(true);

        if (bonus_shield && !hadShield)
            uiManager.ObtainGameplayIcon(false);


        if (bonus_flameThrow)
        {
            AudioManager.instance.PlaySFXLoop(AudioNames.k_lanceFlammeConstant);

            fireAura.gameObject.SetActive(false);
            flameThrow_hitbox.gameObject.SetActive(true);
        }

        //Aura Scale
        Vector3 currentAuraScale = fireAura.localScale;
        currentAuraScale.x = currentAuraScale.z = baseAuraScale + bonus_AuraScale;
        fireAura.localScale = currentAuraScale;

        if(bonus_fireTrail)
            foreach (Transform t in bonus_fireAura)
            {
                currentAuraScale.x = currentAuraScale.z = (baseAuraScale + bonus_AuraScale) / 2;
                t.localScale = currentAuraScale;
                t.gameObject.SetActive(true);
            }

        //Flame Throw Scale
        Vector3 currentFireThrowScale = flameThrow_hitbox.localScale;
        currentFireThrowScale.x = currentFireThrowScale.z = baseFlameThrowScale;
        currentFireThrowScale.y = Mathf.Clamp((baseAuraScale + bonus_AuraScale) / 2, 3, 10);
        flameThrow_hitbox.localScale = currentFireThrowScale;


        //Flame Throw Position
        Vector3 currentFireThrowPos = flameThrow_hitbox.localPosition;
        currentFireThrowPos.z = Mathf.Clamp((baseAuraScale + bonus_AuraScale) / 2, 3, 10);
        flameThrow_hitbox.localPosition = currentFireThrowPos;


    }

    IEnumerator FireTrailCoroutine()
    {
        yield return new WaitForSeconds(fireTrailTimingn);

        bonus_fireAura[trailIndex].position = transform.position;
        trailIndex++;
        if (trailIndex >= bonus_fireAura.Length)
            trailIndex = 0;

        StartCoroutine(FireTrailCoroutine());
    }


    // **************************************** Visuel
    void ApplyVisuel()
    {
        fireMaterial.SetFloat("_Burn_Limit", 1 - (uiManager.remainingTime / 10));

        Vector3 pointPos = Vector3.zero;
        pointPos.y = uiManager.remainingTime / 10;
        firePoint.localPosition = pointPos;
    }

    public void StartTheMeche()
    {
        firePoint.gameObject.SetActive(true);
        AudioManager.instance.PlaySFXOnce(AudioNames.k_matchStart, 3);

        if(!m_source)
            m_source = AudioManager.instance.PlaySFXLoop(AudioNames.k_matchConstant);
        m_source.Play();
    }

    public void DeathRoutine()
    {
        if(!isDead)
            anim.SetTrigger("death");

        useController = false;
        Cursor.visible = true;

        rb.velocity = Vector3.zero;

        m_source.Stop();

        foreach (GameObject gO in onDeathDepop)
            gO.SetActive(false);

        isDead = true;

        deathVcam.Priority = 100;

    }

    public void LoseBonusMatch()
    {
        for (int i = 0; i < currentUpgrades.Count; i++)
        {
            if (currentUpgrades[i].bonus_Match)
            {
                currentUpgrades.RemoveAt(i);
                bonus_match = false;
                return;
            }
        }
    }

    public void LoseBonusShield()
    {
        uiManager.LoseGameplayIcon(false);
        StartCoroutine(SetDashHighFrames());

        for (int i = 0; i < currentUpgrades.Count; i++)
        {
            if (currentUpgrades[i].bonus_Shield)
            {
                currentUpgrades.RemoveAt(i);
                bonus_shield = false;
                return;
            }
        }
    }
}
