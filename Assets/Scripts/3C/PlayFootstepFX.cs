using UnityEngine;

public class PlayFootstepFX : MonoBehaviour
{
    public void PlayFootstep()
    {
        AudioManager.instance.PlaySFXOnce(AudioNames.k_footstep, 0);
    }
}
