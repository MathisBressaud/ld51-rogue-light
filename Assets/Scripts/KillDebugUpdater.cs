using UnityEngine;

public class KillDebugUpdater : MonoBehaviour
{
    void Start()
    {
        GameObject debugUpdater = GameObject.Find("[Debug Updater]");
        if (debugUpdater != null) Destroy(debugUpdater);
    }
}
