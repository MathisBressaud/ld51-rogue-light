using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BoneFire_Altar : MonoBehaviour
{
    CharaUpgrade myUp;
    bool triggerOnce;

    [Title("Bonefire")]
    public BoneFire_V2 bonefire;
    public int myUpgradeIndex;


    [Title("Altar component")]
    public Image icone;
    [Space(10)]
    public GameObject[] VFXs = new GameObject[0];
    [Space(10)]
    public TextMeshProUGUI upgradeTitle_tmp;
    public TextMeshProUGUI upgradeText_tmp;
    [Space(10)]
    public GameObject[] altarVisuels = new GameObject[0];
    public Collider col;


    private void OnEnable()
    {
        triggerOnce = false;
        SetAltarVisuel(true);
    }

    public void SetAltarVisuel(bool enable)
    {
        foreach (GameObject gO in altarVisuels)
            gO.SetActive(enable);

        col.enabled = enable;
    }



    public void SetUpgrade(CharaUpgrade upToSet)
    {
        myUp = upToSet;

        upgradeTitle_tmp.text = myUp.UpgradeName;
        upgradeTitle_tmp.color = myUp.nameColor;

        upgradeText_tmp.text = myUp.TextDescription;
        upgradeText_tmp.color = myUp.textColor;

        for (int i = 0; i < VFXs.Length; i++)
            VFXs[i].SetActive(i == upToSet.VFXindex);

        icone.sprite = upToSet.icon;
        icone.color = upToSet.iconColor;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Constant.k_playerTag))
        {
            triggerOnce = true;

            bonefire.PickUpgrade(myUpgradeIndex);
        }
    }
}
