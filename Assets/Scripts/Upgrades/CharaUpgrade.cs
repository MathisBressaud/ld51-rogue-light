using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Up Script", menuName = "Upgrade/Up player")]
public class CharaUpgrade : ScriptableObject
{
    [Title("Visuels")]
    public string UpgradeName;
    public Color nameColor;
    [TextArea]
    public string TextDescription;
    public Color textColor;
    [Space(10)]
    public Sprite icon;
    public Color iconColor;
    [Space(10)]
    public int VFXindex;





    [Title("Upgrades M�l�e")]
    public float bonus_movespeed;
    public float bonus_AuraScale;
    [Space(10)]
    public bool bonus_flameThrower;
    public bool bonus_fireTrail;

    [Title("Upgrades Distance")]
    public float bonus_bdfCD;
    public float bonus_bdfSpeed;
    public float bonus_bdfScale;
    [Space(10)]
    public bool bonus_piercing;
    public bool bonus_triple;

    [Title("Upgrades Overall")]
    public float bonus_dashCD;
    public float bonus_sloMo;

    [Title("Upgrades Unique")]
    public bool bonus_Match;
    public bool bonus_Shield;
}
