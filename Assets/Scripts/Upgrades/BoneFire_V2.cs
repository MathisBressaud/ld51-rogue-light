using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneFire_V2 : MonoBehaviour
{
    SceneDuJeuManager manager;
    List<CharaUpgrade> upgrades = new List<CharaUpgrade>();

    [Title("Altars")]
    public BoneFire_Altar[] altars = new BoneFire_Altar[2];


    private void Start()
    {
        manager = FindObjectOfType<SceneDuJeuManager>();

        SetupAltars();
    }

    private void OnEnable()
    {
        if (manager == null)
            return;

        SetupAltars();
    }

    void SetupAltars()
    {
        upgrades.Clear();
        upgrades = manager.GenerateUpgrades();

        altars[0].SetUpgrade(upgrades[0]);
        altars[1].SetUpgrade(upgrades[1]);
    }

    public void PickUpgrade(int index)
    {
        foreach (BoneFire_Altar altar in altars)
            altar.SetAltarVisuel(false);

        manager.SelectedUpgrade(index);
    }




}
