using Cinemachine;
using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class SceneDuJeuManager : MonoBehaviour
{
    public bool isAllowedToDie = true;




    public MatchController chara;


    [TabGroup("Camera")] public CinemachineVirtualCamera vCamMap;
    [TabGroup("Camera")] public CinemachineConfiner vCamMapConfiner;
    [Space(10)]
    [TabGroup("Camera")] public CinemachineVirtualCamera vCamPlayer;
    [TabGroup("Camera")] public Transform vConfiner;
    [Space(10)]
    [TabGroup("Camera")] public Vector3 vCamMap_BasePos;
    [TabGroup("Camera")] public float vCamMap_OffSet;
    [Space(10)]
    [TabGroup("Camera")] public float vCamMap_Time2Tween;
    [Space(10)]



    [TabGroup("Corridor")] public Transform tamponNextCorridor;
    [TabGroup("Corridor")] public Transform nextCorridor;
    [TabGroup("Corridor")] public Transform previousCorridor;
    [TabGroup("Corridor")] public float corridor_Time2Tween;

    [TabGroup("Room")] public Transform[] spawnPoints;
    [TabGroup("Room")] public Transform[] nextSpawnPoints;


    [TabGroup("Map Generation")] public LevelGenerator lvlGenerator;
    [TabGroup("Map Generation")] public int currentMap = 0;
    [Space(10)]
    [TabGroup("Map Generation")] public NavMeshSurface elNavmesh;


    [TabGroup("Ennemies")] public Pool projectilePool;
    [TabGroup("Ennemies")] public Pool eyePool;
    [TabGroup("Ennemies")] public Pool[] ennemiPools;
    [Space(10)]
    [TabGroup("Ennemies")] public List<Enemy> currentMobs = new List<Enemy>();
    [TabGroup("Ennemies")] public int remainingMobs;


    [TabGroup("Interface")] public UImanager uiManager;
    [TabGroup("Interface"), SerializeField] private LeaderboardDisplayer m_leaderboardDisplayer;
    [TabGroup("Interface")] public Image m_blackScreen;

    [Title("Slo mo manager")]
    public SlowMotionPostProcess sloMoPP;
    [Space(10)]
    public float currentTimeScale;
    public float sloMo;
    [Space(10)]
    public float time2Slomo_in;
    public float time2Slomo_out;
    Tween sloMoTween;



    [Title("Upgrades Syst�me")]
    public CharaUpgrade infiniteUpgrade;
    public CharaUpgrade[] meleeUpgrade = new CharaUpgrade[0];
    [Space(10)]
    public CharaUpgrade[] distanceUpgrade = new CharaUpgrade[0];
    [Space(10)]
    public CharaUpgrade epicTimer;
    public CharaUpgrade epicShield;
    [Space(10)]
    public CharaUpgrade[] legendaryMeleeUpgrade = new CharaUpgrade[0];
    public CharaUpgrade[] legendaryDistanceUpgrade = new CharaUpgrade[0];

    [Space(10)]
    public List<UpgradeList> currentAllUpgrades = new List<UpgradeList>();
    List<CharaUpgrade> proposedUpdates = new List<CharaUpgrade>();


    [Title("Balancing ennemies Syst�me")]
    public List<EnnemiList> waveTuto = new List<EnnemiList>();
    public List<EnnemiList> waveOfMobs = new List<EnnemiList>();
    bool tutoFinish;
    int currentMobIndex;

    [Title("Modifier Syst�me")]
    public List<LvlModifier> modifList = new List<LvlModifier>();
    public LvlModifier infinteModifier;
    int modifIndex;
    [Space(10)]
    public List<LvlModifier> currentModifiers = new List<LvlModifier>();


    // **************************************** Not a code WARNING
    public bool gameIsRunning;
    private void Start()
    {
        Init();
        ResetUpgrades();
    }

    void Init()
    {
        chara = FindObjectOfType<MatchController>();

        m_blackScreen.DOFade(0f, 1f);


        StartCoroutine(Delay());

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(0.2f);
            lvlGenerator.CreateNewRoom();
            elNavmesh.BuildNavMesh();

            yield return new WaitForSeconds(0.5f);
            OpenNextRoom();
        }

        gameIsRunning = false;
    }

    void ResetUpgrades()
    {
        currentAllUpgrades[0].upgrades.Clear();
        currentAllUpgrades[0].upgrades.AddRange(meleeUpgrade);

        currentAllUpgrades[1].upgrades.Clear();
        currentAllUpgrades[1].upgrades.AddRange(distanceUpgrade);

        currentAllUpgrades[2].upgrades.Clear();
        currentAllUpgrades[2].upgrades.AddRange(legendaryMeleeUpgrade);

        currentAllUpgrades[3].upgrades.Clear();
        currentAllUpgrades[3].upgrades.AddRange(legendaryDistanceUpgrade);
    }



    // **************************************** Code start here fin du WARNING
    // **************************************** Inter Room
    public void PlayerEnterCorridor()
    {
        gameIsRunning = false;

        GenerateNextRoom();

        //uiManager.ResetTimer();
    }

    void GenerateNextRoom()
    {
        lvlGenerator.CreateNewRoom();
        elNavmesh.BuildNavMesh();

        currentMap++;

    }

    // **************************************** Room
    public void PlayerEnterNewRoom()
    {
        ClosePreviousRoom();

        MoveCamera();

        SpawnEnnemies();
    }

    void MoveCamera()
    {
        vCamMapConfiner.enabled = false;

        vConfiner.position = vCamMap_BasePos + Vector3.forward * vCamMap_OffSet * currentMap;

        vCamMap.transform.DOMove(vCamMap_BasePos + Vector3.forward * vCamMap_OffSet * currentMap, vCamMap_Time2Tween).SetEase(Ease.InOutQuad).Play().OnUpdate(() => {
            vCamMapConfiner.enabled = true;
        });

        vCamMap.Priority = 10;
    }

    void SpawnEnnemies()
    {
        currentMobs.Clear();
        remainingMobs = 0;

        if (spawnPoints.Length <= 0)
        {
            gameIsRunning = false;
            OpenNextRoom();

            vCamMap.Priority = 0;
        }
        else
        {
            List<EnnemiList> wave = tutoFinish ? waveOfMobs : waveTuto;

            for (int i = 0; i < wave[currentMobIndex].mobsIndex.Count; i++)
            {
                int index = wave[currentMobIndex].mobsIndex[i];

                GameObject currentSpawnMob = index <= ennemiPools.Length ? ennemiPools[index].GetAvailableObject() : ennemiPools[Random.Range(0, ennemiPools.Length)].GetAvailableObject();

                currentSpawnMob.SetActive(true);

                Enemy currentEnemy = currentSpawnMob.GetComponent<Enemy>();
                currentEnemy.m_projectilePool = projectilePool;
                currentEnemy.m_eyePool = eyePool;

                currentEnemy.m_deathAction = KillEnnemy;
                currentEnemy.m_agent.Warp(spawnPoints[Random.Range(0, spawnPoints.Length)].position);

                currentEnemy.Reset();

                foreach (LvlModifier modif in currentModifiers)
                    modif.ApplyLevelModifier(currentEnemy);

                currentMobs.Add(currentEnemy);
                remainingMobs++;
            }

            currentMobIndex++;
            if ((!tutoFinish && currentMobIndex >= waveTuto.Count) || (tutoFinish && currentMobIndex >= waveOfMobs.Count))
            {
                currentMobIndex = 0;

                if (tutoFinish)
                    ApplyModifier();
                else
                    tutoFinish = true;
            }

            gameIsRunning = true;
            uiManager.StartChrono();
        }

    }

    public void KillEnnemy()
    {
        triggerSloMo();

        remainingMobs -= 1;

        if (remainingMobs <= 0)
        {
            OpenNextRoom();

            vCamMap.Priority = 0;
        }
    }

    void ApplyModifier()
    {
        currentModifiers.Add(modifIndex >= modifList.Count ? infinteModifier : modifList[modifIndex]);
        modifIndex++;
    }

    public List<LvlModifier> GetModifiers()
    {
        return currentModifiers;
    }

    // **************************************** Subscribe from outside the script
    public void SubscribeAsNewSAS(Transform newCorridor)
    {
        previousCorridor = nextCorridor;
        nextCorridor = tamponNextCorridor;
        tamponNextCorridor = newCorridor;
    }

    public void SubscribeAsNewSpawnPoints(Transform[] newSpawns)
    {
        //spawnPoints = newSpawns;
        spawnPoints = nextSpawnPoints;
        nextSpawnPoints = newSpawns;
    }

    // **************************************** Corridor gestion
    void OpenNextRoom()
    {
        nextCorridor.DOLocalMoveY(0, corridor_Time2Tween).SetEase(Ease.InQuad).Play();
    }

    void ClosePreviousRoom()
    {
        previousCorridor.DOLocalMoveY(5, corridor_Time2Tween).SetEase(Ease.InQuad).Play();
    }


    // **************************************** Slo mo manager
    void triggerSloMo()
    {
        sloMoTween.Kill();

        sloMoPP.StartSlowMotionVolume();

        sloMoTween = DOTween.To(() => currentTimeScale, x => currentTimeScale = x, sloMo, time2Slomo_in).SetEase(Ease.OutCubic).Play().OnUpdate(() =>
        {
            Time.timeScale = currentTimeScale;
        }).OnComplete(() =>
        {
            sloMoTween = DOTween.To(() => currentTimeScale, x => currentTimeScale = x, 1, time2Slomo_out).SetEase(Ease.InQuad).Play().OnUpdate(() =>
            {

                Time.timeScale = currentTimeScale;
            }).OnComplete(() => {
                sloMoPP.StopSlowMotionVolume();
            });
        });
    }


    // **************************************** Upgrade generation
    int upgradeGeneration;
    int upThreshold = 3;
    bool epicTime;
    public List<CharaUpgrade> GenerateUpgrades()
    {
        proposedUpdates.Clear();

        upgradeGeneration++;

        if (upgradeGeneration < upThreshold)
        {
            proposedUpdates.Add(currentAllUpgrades[0].upgrades.Count >= 0 ? currentAllUpgrades[0].upgrades[Random.Range(0, currentAllUpgrades[0].upgrades.Count)] : infiniteUpgrade);
            proposedUpdates.Add(currentAllUpgrades[1].upgrades.Count >= 0 ? currentAllUpgrades[1].upgrades[Random.Range(0, currentAllUpgrades[1].upgrades.Count)] : infiniteUpgrade);
        }
        else
        {
            upgradeGeneration = 0;
            upThreshold++;

            if (epicTime)
            {
                proposedUpdates.Add(currentAllUpgrades[2].upgrades.Count >= 0 ? currentAllUpgrades[2].upgrades[Random.Range(0, currentAllUpgrades[2].upgrades.Count)] : infiniteUpgrade);
                proposedUpdates.Add(currentAllUpgrades[3].upgrades.Count >= 0 ? currentAllUpgrades[3].upgrades[Random.Range(0, currentAllUpgrades[3].upgrades.Count)] : infiniteUpgrade);
            }
            else
            {
                proposedUpdates.Add(chara.currentUpgrades.Contains(epicTimer) ? infiniteUpgrade : epicTimer);
                proposedUpdates.Add(chara.currentUpgrades.Contains(epicShield) ? infiniteUpgrade : epicShield);
            }

            epicTime = !epicTime;
        }

        return proposedUpdates;
    }

    public void SelectedUpgrade(int index)
    {
        chara.SetNewUpgrade(proposedUpdates[index]);

        if (currentAllUpgrades[0].upgrades.Contains(proposedUpdates[index]))
            currentAllUpgrades[0].upgrades.Remove(proposedUpdates[index]);

        if (currentAllUpgrades[1].upgrades.Contains(proposedUpdates[index]))
            currentAllUpgrades[1].upgrades.Remove(proposedUpdates[index]);

        if (currentAllUpgrades[2].upgrades.Contains(proposedUpdates[index]))
            currentAllUpgrades[2].upgrades.Remove(proposedUpdates[index]);

        if (currentAllUpgrades[3].upgrades.Contains(proposedUpdates[index]))
            currentAllUpgrades[3].upgrades.Remove(proposedUpdates[index]);
    }

    // **************************************** DEATH

    public void Death()
    {
        if (!isAllowedToDie)
            return;

        gameIsRunning = false;
        sloMoTween.Kill();
        Time.timeScale = 1f;
        chara.DeathRoutine();

        PlayerPrefs.SetInt(Constant.k_lastScore, currentMap);
        //m_leaderboardDisplayer.gameObject.SetActive(true);

        StartCoroutine(LoadAsync());

        IEnumerator LoadAsync()
        {
            //var operation = SceneManager.LoadSceneAsync(Constant.k_leaderboardScene);

            //operation.allowSceneActivation = false;

            m_blackScreen.DOFade(1f, 3f);

            uiManager.ResetScreenFire(true);
            yield return new WaitForSeconds(3f);

            SceneManager.LoadScene(Constant.k_leaderboardScene);

            //yield return new WaitUntil(() => operation.progress >= 0.9f);

            //operation.allowSceneActivation = true;
        }

    }


}

[System.Serializable]
public class UpgradeList
{
    public List<CharaUpgrade> upgrades = new List<CharaUpgrade>();
}

[System.Serializable]
public class EnnemiList
{
    public List<int> mobsIndex = new List<int>();
}