using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class UImanager : MonoBehaviour
{
    SceneDuJeuManager manager;


    [TabGroup("UI Timer")] public float remainingTime = 10;
    [Space(10)]
    [TabGroup("UI Timer")] public TextMeshProUGUI timerTMP;
    [TabGroup("UI Timer")] public TextMeshProUGUI stageTMP;
    [TabGroup("UI Timer")] public TextMeshProUGUI ennemiesTMP;
    [Space(10)]
    [TabGroup("UI Timer")] public GameObject matchUp;
    [TabGroup("UI Timer")] public GameObject shieldUp;


    [Title("Shader")]
    public Material screenFire;

    [Title("Screen Anims")]
    public float maxScale;
    public float scale_time2tween_In;
    public float scale_time2tween_Out;
    [Space(10)]
    public Color baseCol;
    public Color tweenCol;
    [Space(10)]
    public float warningTime;
    //public float time2tweenTimer;

    private AudioSource m_source;

    Vector3 savePos;
    private void Start()
    {
        manager = FindObjectOfType<SceneDuJeuManager>();

        savePos = timerTMP.transform.position;
        ResetScreenFire(false);
    }

    bool dead;
    public void ResetScreenFire(bool isDead)
    {
        Debug.Log("yolo");
        if (isDead)
            dead = true;

        screenFire.SetFloat("_NoiseOpacity", 0);
        screenFire.SetFloat("_DistortionOpacity", 0);
    }

    private void Update()
    {
        TimeManager();
        UpdateUI();
    }

    // **************************************** Timer uses
    bool triggerOnce;
    bool warning;
    void TimeManager()
    {
        if(manager.gameIsRunning)
            remainingTime -= Time.deltaTime;

        remainingTime = Mathf.Clamp(remainingTime, 0, 10.9f);

        if (!warning && remainingTime < warningTime)
        {
            warning = true;
            Warning();
        }

            float noise = Mathf.Clamp(1 - (remainingTime / 10) - 0.5f, 0, 1);

        if (!dead)
        {
            screenFire.SetFloat("_NoiseOpacity", noise * 2);
            screenFire.SetFloat("_DistortionOpacity", noise * 2);
        }

        if(!m_source)
            m_source = AudioManager.instance.PlaySFXLoop(AudioNames.k_tension);
        else if(!m_source.isPlaying)
        {
            m_source.time = 0;
            m_source.Play();
        }

        m_source.volume = Mathf.Clamp(noise * 2, 0, 1);

        if(noise == 0)
        {
            m_source.time = 0;
        }


        if (remainingTime <= 0 && !triggerOnce)
        {
            if(manager.chara.bonus_match)
            {
                manager.chara.LoseBonusMatch();
                LoseGameplayIcon(true);
                ResetTimer();
                return;
            }

            triggerOnce = true;
            m_source.Stop();
            manager.Death();
        }
    }

    // **************************************** Update de l'UI
    void UpdateUI()
    {
        timerTMP.text = Mathf.FloorToInt(remainingTime).ToString();

        stageTMP.text = manager.currentMap.ToString();
        ennemiesTMP.text = manager.remainingMobs.ToString();
    }



    // **************************************** External Functions
    bool gameIsStart;
    public void StartTimer()
    {
        gameIsStart = true;

        timerTMP.gameObject.SetActive(true);
        stageTMP.gameObject.SetActive(true);
        ennemiesTMP.gameObject.SetActive(true);

        manager.chara.StartTheMeche();

        PunchThisTimer();
    }

    public void ResetTimer()
    {
        if (!gameIsStart)
            StartTimer();

        warning = false;
        remainingTime = 10.9f;

        timerTMP.DOKill();
        timerTMP.transform.DOKill();
        timerTMP.transform.position = savePos;
        timerTMP.transform.localScale = Vector3.one;
        timerTMP.color = baseCol;
    }

    public void StartChrono()
    {
        ResetTimer();
        PunchThisTimer();
    }

    void PunchThisTimer()
    {
        timerTMP.DOColor(tweenCol, scale_time2tween_In).SetEase(Ease.OutQuad).Play();
        timerTMP.transform.DOScale(Vector3.one * maxScale, scale_time2tween_In).SetEase(Ease.OutQuad).Play().OnComplete(() => {

            timerTMP.DOColor(baseCol, scale_time2tween_Out).SetEase(Ease.OutQuad).Play();
            timerTMP.transform.DOScale(Vector3.one, scale_time2tween_Out).SetEase(Ease.InQuad).Play();
        });
    }


    void Warning()
    {
        timerTMP.transform.DOScale(Vector3.one * maxScale, scale_time2tween_In).SetEase(Ease.OutQuad).Play().OnComplete(() => {
            timerTMP.transform.DOScale(Vector3.one, 1 - scale_time2tween_In).SetEase(Ease.InQuad).Play().OnComplete(() =>
            {
                timerTMP.transform.DOScale(Vector3.one * maxScale, scale_time2tween_In).SetEase(Ease.OutQuad).Play().OnComplete(() => {
                    timerTMP.transform.DOScale(Vector3.one, 1 - scale_time2tween_In).SetEase(Ease.InQuad).Play().OnComplete(() =>
                    {
                        timerTMP.transform.DOScale(Vector3.one * maxScale, scale_time2tween_In).SetEase(Ease.OutQuad).Play().OnComplete(() => {
                            timerTMP.transform.DOScale(Vector3.one, 1 - scale_time2tween_In).SetEase(Ease.InQuad).Play().OnComplete(() =>
                            {
                                timerTMP.transform.DOShakePosition(1, 200, 100).SetEase(Ease.InOutQuad).Play();
                            });
                        });
                    });
                });
            });
        });
    }

    public void ObtainGameplayIcon(bool match)
    {
        GameObject obj = match ? matchUp : shieldUp;

        obj.SetActive(true);
        obj.transform.DOScale(Vector3.one * maxScale, scale_time2tween_In).SetEase(Ease.OutQuad).Play().OnComplete(() => {
            obj.transform.DOScale(Vector3.one, scale_time2tween_Out).SetEase(Ease.InQuad).Play();
        });
    }

    public void LoseGameplayIcon(bool match)
    {
        GameObject obj = match ? matchUp : shieldUp;
        obj.transform.DOShakePosition(0.5f, 100, 50).SetEase(Ease.InOutQuad).Play().OnComplete(() => {
            obj.SetActive(false);
        });

    }
}
