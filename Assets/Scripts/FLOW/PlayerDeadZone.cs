using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeadZone : MonoBehaviour
{
    SceneDuJeuManager manager;

    private void Start()
    {
        manager = FindObjectOfType<SceneDuJeuManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Constant.k_playerTag))
        {
            if (other.GetComponent<MatchController>().isInvulnerable)
                return;

            AudioManager.instance.PlaySFXOnce(AudioNames.k_plouf, 7);

            if (manager.chara.bonus_shield)
            {
                manager.chara.LoseBonusShield();
            }
            else
                manager.Death();
        }
    }
}
