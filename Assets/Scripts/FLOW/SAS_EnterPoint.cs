using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SAS_EnterPoint : SAS
{
    [Title("Flamme")]
    public Transform tweenFlame;
    public Vector3 flamePos;
    [Space(10)]
    public float timeBeforeTween;
    public float timeToTween;
    
    public override void SetSAS()
    {
        base.SetSAS();
        tweenFlame.gameObject.SetActive(true);
        tweenFlame.localPosition = flamePos;
    }

    public override void TriggerSas()
    {
        base.TriggerSas();

        manager.PlayerEnterCorridor();

        StartCoroutine(Delay());

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(timeBeforeTween);

            tweenFlame.DOMove(manager.chara.firePoint.position, timeToTween).SetEase(Ease.InOutQuad).Play().OnComplete(() => {
                tweenFlame.gameObject.SetActive(false);
                manager.uiManager.ResetTimer();
                AudioManager.instance.PlaySFXOnce(AudioNames.k_matchStart, 3);
            });
        }
    }
}
