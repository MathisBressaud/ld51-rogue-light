using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SAS_ExistPoint : SAS
{
    public override void SetSAS()
    {
        base.SetSAS();
    }

    public override void TriggerSas()
    {
        base.TriggerSas();
        manager.PlayerEnterNewRoom();
    }
}
