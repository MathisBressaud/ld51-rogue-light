using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ROOM : MonoBehaviour
{
    SceneDuJeuManager manager;

    public Transform[] spawnPoints;

    public Lazer[] lazers;

    public GameObject labyv1;
    public GameObject labyv2;

    void Start()
    {
        manager = FindObjectOfType<SceneDuJeuManager>();

        SetRoom();
    }

    public void OnEnable()
    {
        if (manager == null)
            return;

        SetRoom();
    }

    void SetRoom()
    {
        manager.SubscribeAsNewSpawnPoints(spawnPoints);

        foreach (Lazer l in lazers)
            l.gameObject.SetActive(false);

        if (labyv1 != null && labyv2 != null)
        {
            labyv1.SetActive(false);
            labyv2.SetActive(false);
        }

        ApplyModifier(manager.GetModifiers());
    }

    void ApplyModifier(List<LvlModifier> modifiers)
    {
        foreach (LvlModifier modif in modifiers)
        {
            foreach (Lazer l in lazers)
                modif.ApplyLevelModifier(l);

            modif.ApplyLevelModifier(labyv1, labyv2);
        }
    }
}
