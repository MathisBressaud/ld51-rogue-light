using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lazer : MonoBehaviour
{
    SceneDuJeuManager manager;
    
    public float lazerDuration;
    public float lazerSign;
    public float lazerWait;
    [Space(10)]
    public Vector3 scaleNul;
    public Vector3 scaleSign;
    public Vector3 scaleBeam;
    [Space(10)]
    public Transform beam;
    public Transform dirbeam;
    [Space(10)]
    public GameObject beamHitbox;
    public Transform beamVisuel;

    AudioSource m_source;


    [Space(10)]
    RaycastHit hit;
    public LayerMask hitable;


    public void Start()
    {
        manager = FindObjectOfType<SceneDuJeuManager>();

        m_source = AudioManager.instance.PlaySFXLoop(AudioNames.k_laser);
        m_source.Stop();

        //ResetLazer();
    }

    private void Update()
    {
        SetDistance();
    }

    void SetDistance()
    {
        if (Physics.Raycast(beam.position, dirbeam.position - beam.position, out hit, 50, hitable))
        {
            Vector3 scale = beam.localScale;
            scale.z = Vector3.Distance(hit.point, beam.transform.position) / 10;

            beam.localScale = scale;
        }
    }


    void ResetLazer()
    {
        StopAllCoroutines();
        
        beamHitbox.SetActive(false);
        beamVisuel.gameObject.SetActive(false);
        beamVisuel.localScale = scaleNul;
    }

    public void SetLazer(bool isInfinite)
    {
        ResetLazer();

        if (isInfinite)
        {
            beamHitbox.SetActive(true);
            beamVisuel.gameObject.SetActive(true);
            beamVisuel.localScale = scaleBeam;
        }
        else
        {
            StartCoroutine(LazerCorroutine());
        }

        //SetDistance();
    }

    IEnumerator LazerCorroutine()
    {
        yield return new WaitForSeconds(lazerWait);
        beamVisuel.gameObject.SetActive(true);
        beamVisuel.DOScale(scaleSign, 0.14f).SetEase(Ease.InOutQuad).Play();

        yield return new WaitForSeconds(lazerSign);
        m_source.Play();
        beamHitbox.SetActive(true);
        beamVisuel.DOScale(scaleBeam, 0.14f).SetEase(Ease.InOutQuad).Play();

        yield return new WaitForSeconds(lazerDuration);
        m_source.Stop();
        beamHitbox.SetActive(false);
        beamVisuel.DOScale(scaleNul, 0.14f).SetEase(Ease.InOutQuad).Play().OnComplete(() => {
            beamVisuel.gameObject.SetActive(false);
            StartCoroutine(LazerCorroutine());
        });
    }
}
