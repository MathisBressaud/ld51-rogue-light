using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SAS : MonoBehaviour
{
    public Transform wholeSAS;

    protected SceneDuJeuManager manager;

    bool triggerOnce;

    public void Start()
    {
        manager = FindObjectOfType<SceneDuJeuManager>();
        SetSAS();
    }

    public void OnEnable()
    {
        if (manager == null)
            return;

        SetSAS();
    }

    public virtual void SetSAS()
    {
        triggerOnce = false;
        if (wholeSAS != null)
            manager.SubscribeAsNewSAS(wholeSAS);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!triggerOnce && other.CompareTag(Constant.k_playerTag))
            TriggerSas();

    }

    public virtual void TriggerSas()
    {
        triggerOnce = true;
    }
}
