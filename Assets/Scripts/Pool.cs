﻿using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    //Variables
    [SerializeField]
    GameObject m_object;

    List<GameObject> pool = new List<GameObject>();

    [SerializeField]
    int poolSize = 100;


    //Initialisation
    void Awake()
    {
        SpawnObjects();
    }

    void SpawnObjects()
    {
        //Create Objects
        for (int i = 0; i < poolSize; i++)
        {
            GameObject currentObject;
            currentObject = Instantiate(m_object);
            currentObject.SetActive(false);
            pool.Add(currentObject);
        }
    }

    public GameObject GetAvailableObject()
    {
        //Check for an object available
        for (int i = 0; i < pool.Count; i++)
        {
            if (!pool[i].activeInHierarchy)
            {
                return pool[i];
            }
        }

        //No Object Available
        GameObject newObject;
        newObject = Instantiate(m_object);
        newObject.SetActive(false);
        pool.Add(newObject);
        poolSize += 1;

        return newObject;
    }

    public GameObject Instantiate(Vector3 position, Quaternion rotation, Transform parent = null)
    {
        GameObject currentObject = GetAvailableObject();

        currentObject.transform.position = position;
        currentObject.transform.rotation = rotation;
        currentObject.transform.parent = parent;

        currentObject.SetActive(true);

        return currentObject;
    }
}