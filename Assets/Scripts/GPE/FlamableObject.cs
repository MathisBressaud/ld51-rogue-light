using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using System.Collections;
using DG.Tweening;

public class FlamableObject : MonoBehaviour, IFlammable
{
    public bool IsFlamed { get; set; }

    [TabGroup("Parameters"), SerializeField] private float m_burnDuration = 3f;

    private Material m_dissolveMat;

    //Events
    [TabGroup("Events"), SerializeField] protected UnityEvent OnLitStart;
    [TabGroup("Events"), SerializeField] protected UnityEvent OnLitEnd;

    [PropertySpace]

    private void Start()
    {
        m_dissolveMat = GetComponentInChildren<MeshRenderer>().material;
    }

    public void Reset()
    {
        if (IsFlamed)
        {
            IsFlamed = false;
            OnLitEnd?.Invoke();

            m_dissolveMat.SetFloat(Constant.k_dissolveAmount, 0);
        }
    }

    [Button]
    public void Lit()
    {
        if(!IsFlamed)
        {
            IsFlamed = true;
            OnLitStart?.Invoke();

            AudioManager.instance.PlaySFXOnce(AudioNames.k_fireStart, 2);

            m_dissolveMat.DOFloat(1, Constant.k_dissolveAmount, m_burnDuration).SetEase(Ease.InSine).Play();

            StartCoroutine(Delay());
        }

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(m_burnDuration);

            gameObject.SetActive(false);
        }
    }
}
