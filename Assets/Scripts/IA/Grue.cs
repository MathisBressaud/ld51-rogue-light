using UnityEngine;
using DG.Tweening;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine.Animations;

public class Grue : Enemy
{
    [PropertySpace]

    [TabGroup("Movement"), SerializeField] private float m_idealStayRange = 15;

    [TabGroup("Movement"), SerializeField] private float m_fleeRange = 5;

    [TabGroup("Movement"), SerializeField] private float m_calculateDestinationMaxAngle = 15f;

    [PropertySpace]

    [TabGroup("Movement"), SerializeField] private float m_changeDirectionDuration = 1f;
    private float m_changeDirectionNextTime = 0;

    [PropertySpace]

    [TabGroup("Movement"), SerializeField] protected bool m_canMoveWhileAttacking;

    [TabGroup("Références"), SerializeField] protected Transform m_attackBaseTransformSecondary;


    public override void Reset()
    {
        base.Reset();

        m_changeDirectionNextTime = 0;
    }

    protected override void Move()
    {
        if (m_changeDirectionNextTime < Time.time)
        {
            Vector3 destination = m_target.position + (Quaternion.AngleAxis(Random.Range(-m_calculateDestinationMaxAngle, m_calculateDestinationMaxAngle), Vector3.up) * (transform.position - m_target.position).normalized * m_idealStayRange);
            destination.y = transform.position.y;

            m_agent.SetDestination(destination);

            m_changeDirectionNextTime = Time.time + m_changeDirectionDuration;
        }

        //m_animator.SetBool(Constant.k_animatorRun, m_agent.velocity.magnitude > 0.01f);

        var dist = Vector3.Distance(transform.position, m_target.position);
        if (dist < m_attackRange * (1 + m_attackRange_bonus) && dist > m_fleeRange)
        {
            //if(!m_canMoveWhileAttacking)
            //    m_animator.SetBool(Constant.k_animatorRun, false);

            m_currentAction = Attack;
        }
    }

    protected override void AttackOnce()
    {
        StartCoroutine(DelayAttack());

        IEnumerator DelayAttack()
        {
            isAttacking = true;

            OnAttackStart?.Invoke();

            yield return new WaitForSeconds(m_attackStartWaitTime + m_attackStartWaitTime_bonus);

            m_animator.SetBool(Constant.k_animatorAtk, true);

            for (int i = 0; i < m_projectileNumber + m_projectileNumber_bonus; i++)
            {
                Vector3 attackDirection = m_target.position - m_attackBaseTransform.position;
                attackDirection.y = 0;
                attackDirection = attackDirection.normalized;

                //Vector3 attackDirectionSecondary = m_target.position - m_attackBaseTransformSecondary.position;
                //attackDirectionSecondary.y = 0;
                //attackDirectionSecondary = attackDirectionSecondary.normalized;

                var currentProjectile = m_projectilePool.Instantiate(m_attackBaseTransform.position, Quaternion.identity);
                var currentProjectileSecondary = m_projectilePool.Instantiate(m_attackBaseTransformSecondary.position, Quaternion.identity);
                currentProjectile.SetActive(true);
                currentProjectileSecondary.SetActive(true);

                Vector3 currentDirection = Quaternion.AngleAxis(m_angleOffset * (i % 2 == 0 ? -i / 2 : (i + 1) / 2), Vector3.up) * attackDirection;
                Vector3 currentDirectionSecondary = Quaternion.AngleAxis(m_angleOffset * (i % 2 == 0 ? i / 2 : -(i + 1) / 2), Vector3.up) * attackDirection;

                //A changer
                currentProjectile.GetComponent<Rigidbody>().velocity = currentDirection * m_projectileSpeed;
                currentProjectileSecondary.GetComponent<Rigidbody>().velocity = currentDirectionSecondary * m_projectileSpeed;

                OnShoot?.Invoke();

                AudioManager.instance.PlaySFXOnce(AudioNames.k_shootEnemy, 8);

                if (m_durationOffset != 0 && !m_canMoveWhileAttacking)
                {
                    Recoil(currentDirection + currentDirectionSecondary);
                }

                yield return new WaitForSeconds(m_durationOffset);
            }

            m_animator.SetBool(Constant.k_animatorAtk, false);

            OnAttackEnd?.Invoke();

            yield return new WaitForSeconds(m_attackEndWaitTime);

            if (!m_canMoveWhileAttacking)
            {
                m_agent.enabled = true;
                m_agent.isStopped = false;
            }

            m_currentAction = Move;

            isAttacking = false;
        }
    }
}
