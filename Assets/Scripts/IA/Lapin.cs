using UnityEngine;

public class Lapin : Enemy
{
    protected override void Move()
    {
        m_agent.SetDestination(m_target.position);

        m_animator.SetBool(Constant.k_animatorRun, m_agent.velocity.magnitude > 0.01f);

        if(Vector3.Distance(transform.position, m_target.position) < m_attackRange * (1 + m_attackRange_bonus))
        {
            m_animator.SetBool(Constant.k_animatorRun, false);
            m_currentAction = Attack;
        }
    }
}