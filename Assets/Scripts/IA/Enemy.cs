using UnityEngine;
using UnityEngine.AI;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.Animations;
using DG.Tweening;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour, IFlammable
{
    public delegate void Action();
    protected Action m_currentAction;
    public Action m_deathAction;

    //Parameters
    public bool IsFlamed { get; set; }

    [TabGroup("Attack Parameters"), SerializeField] protected float m_attackRange = 15;
    [TabGroup("Attack Parameters")] public float m_attackRange_bonus;

    [PropertySpace]

    [TabGroup("Attack Parameters"), SerializeField] protected float m_attackStartWaitTime = 0.25f;
    [TabGroup("Attack Parameters")] public float m_attackStartWaitTime_bonus;
    [TabGroup("Attack Parameters"), SerializeField] protected float m_attackEndWaitTime = 0.5f;

    [PropertySpace]

    [TabGroup("Attack Parameters"), SerializeField] protected float m_attackRecoilDistance = 0f;
    [TabGroup("Attack Parameters"), SerializeField] protected float m_attackRecoilDuration = 0.1f;

    [PropertySpace]

    [TabGroup("Attack Parameters"), SerializeField] protected int m_projectileNumber = 1;
    [TabGroup("Attack Parameters")] public int m_projectileNumber_bonus;

    [TabGroup("Attack Parameters"), SerializeField] protected float m_angleOffset = 5;
    [TabGroup("Attack Parameters"), SerializeField] protected float m_durationOffset = 0;

    [PropertySpace]

    [TabGroup("Attack Parameters"), SerializeField] protected float m_projectileSpeed = 3;

    [TabGroup("Movement"), SerializeField] private float m_runawayDistance = 10;
    [TabGroup("Movement"), SerializeField] private float m_runawayChangingDirectionDuration = 1;
    private float m_runawayChangingDirectionNextTime = 0;

    protected bool isAttacking;

    [PropertySpace]

    [TabGroup("Spawn"), SerializeField] private Vector2 m_spawnTimeMinMax = new Vector2(0.5f, 2f);
    [TabGroup("Spawn"), SerializeField] private float m_spawnHeight = 20f;
    private float m_spawnDuration;
    private bool m_isSpawning;

    //Component
    [HideInInspector] public NavMeshAgent m_agent;

    //Références
    protected Transform m_target;

    [TabGroup("Références")] public Pool m_projectilePool;
    [TabGroup("Références")] public Pool m_eyePool;
    [TabGroup("Références"), SerializeField] protected Animator m_animator;
    [TabGroup("Références"), SerializeField] protected LookAtConstraint m_lookAtConstraint;
    [TabGroup("Références"), SerializeField] protected Collider m_collider;

    [PropertySpace]

    [TabGroup("Références"), SerializeField] protected MeshRenderer[] m_renderersToDissolve;

    [PropertySpace]

    [TabGroup("Références"), SerializeField] protected Transform m_eyesTransform;
    [TabGroup("Références"), SerializeField] protected Transform m_attackBaseTransform;

    [PropertySpace]

    //Events
    [TabGroup("Events"), SerializeField] protected UnityEvent OnAttackStart;
    [TabGroup("Events"), SerializeField] protected UnityEvent OnAttackEnd;
    [TabGroup("Events"), SerializeField] protected UnityEvent OnShoot;

    [PropertySpace]

    [TabGroup("Events"), SerializeField] protected UnityEvent OnLitStart;
    [TabGroup("Events"), SerializeField] protected UnityEvent OnLitEnd;

    [PropertySpace]

    [TabGroup("Events"), SerializeField] protected UnityEvent OnDeath;


    protected virtual void Awake()
	{
        m_agent = GetComponent<NavMeshAgent>();
        m_target = GameObject.FindGameObjectWithTag(Constant.k_playerTag).transform;

        if (!m_target) Debug.LogError("Couldn't find object with tag Player");

        var lookAtSource = new ConstraintSource();
        lookAtSource.sourceTransform = m_target;
        lookAtSource.weight = 1;

        m_lookAtConstraint.AddSource(lookAtSource);
        m_lookAtConstraint.constraintActive = true;

        m_currentAction = Spawning;
    }

    protected virtual void FixedUpdate()
    {
        m_currentAction.Invoke();
    }

    public virtual void Reset()
    {
        if (IsFlamed)
        {
            IsFlamed = false;
            OnLitEnd?.Invoke();
        }

        m_attackRange_bonus = 0;
        m_attackStartWaitTime_bonus = 0;
        m_projectileNumber_bonus = 0;

        DOTween.Kill(gameObject);
        StopAllCoroutines();

        isAttacking = false;
        //Setup spawn
        m_agent.enabled = false;
        m_collider.enabled = false;
        m_spawnDuration = Random.Range(m_spawnTimeMinMax.x, m_spawnTimeMinMax.y);
        m_lookAtConstraint.transform.localPosition = Vector3.up * m_spawnHeight * m_spawnDuration;

        for (int i = 0; i < m_renderersToDissolve.Length; i++)
            m_renderersToDissolve[i].material.SetFloat(Constant.k_dissolveAmount, 0);

        m_lookAtConstraint.constraintActive = true;

        m_animator.SetBool(Constant.k_animatorRun, false);

        m_runawayChangingDirectionNextTime = 0;

        m_currentAction = Spawning;
    }

    protected virtual void Spawning()
    {
        if (!m_isSpawning)
        {
            m_isSpawning = true;

            m_lookAtConstraint.transform.DOLocalMove(Vector3.zero, m_spawnDuration).SetEase(Ease.Linear).Play().OnComplete(() => EndSpawnReset());
        }
    }

    private void EndSpawnReset()
    {
        m_collider.enabled = true;

        m_agent.enabled = true;
        m_agent.Warp(transform.position);
        m_agent.isStopped = false;
        m_isSpawning = false;

        m_currentAction = Move;
    }

    protected virtual void Move()
    {
    }

    protected virtual void Attack()
    {
        if (!isAttacking)
        {
            Vector3 dirToTarget = m_target.position - transform.position;
            dirToTarget.y = 0;

            if (!Physics.Raycast(transform.position, dirToTarget, dirToTarget.magnitude, LayerMask.GetMask(Constant.k_layerNavMeshable)))
            {
                AttackOnce();
            }
            else
            {
                m_currentAction = Move;
            }
        }
    }

    protected virtual void AttackOnce()
    {
        StartCoroutine(DelayAttack());

        IEnumerator DelayAttack()
        {
            isAttacking = true;

            //Vector3 lookPosition = m_target.position;
            //lookPosition.y = transform.position.y;

            m_agent.isStopped = true;
            m_agent.enabled = false;

            //transform.DODynamicLookAt(lookPosition, m_attackStartWaitTime).Play();

            OnAttackStart?.Invoke();

            yield return new WaitForSeconds(m_attackStartWaitTime + m_attackStartWaitTime_bonus);

            m_animator.SetTrigger(Constant.k_animatorAtk);

            Vector3 attackDirection = m_target.position - m_attackBaseTransform.position;
            attackDirection.y = 0;
            attackDirection = attackDirection.normalized;


            if (m_durationOffset == 0)
            {
                Recoil(attackDirection);

                AudioManager.instance.PlaySFXOnce(AudioNames.k_shootEnemy, 8);
            }

            for (int i = 0; i < m_projectileNumber + m_projectileNumber_bonus; i++)
            {
                var currentProjectile = m_projectilePool.Instantiate(m_attackBaseTransform.position, Quaternion.identity);
                currentProjectile.SetActive(true);

                Vector3 currentDirection = Quaternion.AngleAxis(m_angleOffset * (i % 2 == 0 ? -i / 2 : (i + 1) / 2), Vector3.up) * attackDirection;

                //A changer
                currentProjectile.GetComponent<Rigidbody>().velocity = currentDirection * m_projectileSpeed;

                OnShoot?.Invoke();

                if (m_durationOffset != 0)
                {
                    Recoil(currentDirection);

                    AudioManager.instance.PlaySFXOnce(AudioNames.k_shootEnemy, 8);
                }

                yield return new WaitForSeconds(m_durationOffset);
            }

            OnAttackEnd?.Invoke();

            yield return new WaitForSeconds(m_attackEndWaitTime);

            m_agent.enabled = true;
            m_agent.isStopped = false;

            m_currentAction = Move;

            isAttacking = false;
        }
    }

    protected void Recoil(Vector3 attackDirection)
    {
        Vector3 dest;
        RaycastHit hit;
        NavMeshPath path = new NavMeshPath();

        float currentDuration = m_attackRecoilDuration;

        if (Physics.Raycast(transform.position, -attackDirection, out hit, m_attackRecoilDistance, LayerMask.GetMask(Constant.k_layerDefaut, Constant.k_layerNavMeshable)))
        {
            dest = hit.point;
            dest.y = transform.position.y;
            currentDuration = m_attackRecoilDuration * (Vector3.Distance(dest, transform.position)) / m_attackRecoilDistance;
        }
        else
        {
            dest = transform.position - (attackDirection * m_attackRecoilDistance);
            dest.y = transform.position.y;
        }

        m_agent.enabled = true;
        m_agent.SetDestination(dest);
        m_agent.enabled = false;

        dest = m_agent.destination.x != Mathf.Infinity? m_agent.destination : dest;
        dest.y = transform.position.y;

        transform.DOMove(dest, currentDuration).SetEase(Ease.OutSine).Play();
    }

    [PropertySpace]

    [Button]
    public virtual void Lit()
    {
        if (!IsFlamed && !m_isSpawning)
        {
            IsFlamed = true;
            OnLitStart?.Invoke();
            m_currentAction = RunAway;

            DOTween.Kill(gameObject);
            StopAllCoroutines();

            AudioManager.instance.PlaySFXOnce(AudioNames.k_fireStart, 2);

            isAttacking = false;
            m_agent.enabled = true;
            m_agent.isStopped = false;

            m_lookAtConstraint.constraintActive = false;
            m_lookAtConstraint.transform.localEulerAngles = Vector3.zero;

            //Call game managers
            m_deathAction?.Invoke();

            for (int i = 0; i < m_renderersToDissolve.Length; i++)
                m_renderersToDissolve[i].material.DOFloat(1, Constant.k_dissolveAmount, Constant.k_waitToDieWhenLit).SetEase(Ease.InSine).Play();

            StartCoroutine(Delay());
        }

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(Constant.k_waitToDieWhenLit);

            Death();
        }
    }

    public void Death()
    {
        OnDeath?.Invoke();

        for(int i = 0; i < 2; i++)
        {
            m_eyePool.Instantiate(m_eyesTransform.position + (transform.right * 0.25f * (2 * i - 1)), Quaternion.identity);
        }

        gameObject.SetActive(false);
    }

    protected virtual void RunAway()
    {
        if(m_runawayChangingDirectionNextTime < Time.time)
        {
            m_agent.SetDestination(transform.position + ((m_target.position - transform.position).normalized * m_runawayDistance));
            m_animator.SetBool(Constant.k_animatorRun, m_agent.velocity.magnitude > 0.01f);
            m_runawayChangingDirectionNextTime = Time.time + m_runawayChangingDirectionDuration;
        }
    }
}