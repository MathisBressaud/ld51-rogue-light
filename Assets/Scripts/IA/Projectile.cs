using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Projectile : MonoBehaviour
{
    [SerializeField] private int damage = 1;
    [SerializeField] private int duration = 10;

    [SerializeField] private Collider m_collider;
    [SerializeField] private GameObject m_renderer;

    [Space]

    [SerializeField] private UnityEvent OnExplosion;
    [SerializeField] private UnityEvent OnExplosionPlayer;

    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(Delay());

        m_renderer.SetActive(true);
        m_collider.enabled = true;

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(duration);
            
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Constant.k_playerTag)
        {
            MatchController chara = other.GetComponent<MatchController>();


            if (chara.isInvulnerable)
                return;

            AudioManager.instance.PlaySFXOnce(AudioNames.k_hitWater, 9);

            OnExplosionPlayer?.Invoke();

            //Ne me tue pas mathis pliz

            if (chara.bonus_shield)
            {
                chara.LoseBonusShield();
            }
            else
                FindObjectOfType<SceneDuJeuManager>().Death();

        }
        else
        {
            OnExplosion?.Invoke();
        }

        StartCoroutine(Delay());

        IEnumerator Delay()
        {
            m_renderer.SetActive(false);
            m_collider.enabled = false;

            yield return new WaitForSeconds(5f);

            gameObject.SetActive(false);
        }
    }
}
